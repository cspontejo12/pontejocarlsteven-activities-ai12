import React from 'react';
import { View, Text, StyleSheet, Image, useWindowDimensions } from 'react-native';



export default Homeitem = ({ item }) => {

  const { width } = useWindowDimensions();

  return (
    <View style={[styles.container, { width }]}>
      <Image source={item.image} style={[styles.image, { width, resizeMode: 'contain' }]} />

      <View style={{ flex: 0.3 }}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    paddingBottom: 5,
    borderRadius: 15,
    flex: 0.7,
    justifyContent: 'center',

  },
  title: {
    fontWeight: '800',
    fontSize: 28,
    marginBottom: 10,
    color: 'mediumvioletred',
    textAlign: 'center',
  },
  description: {
    fontWeight: '700',
    color: 'mediumpurple',
    textAlign: 'center',
    paddingHorizontal: 64,
  },
  button: {
    flex: 1,
    height: 50,
    borderRadius: 5,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
