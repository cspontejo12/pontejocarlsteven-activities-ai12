import React, { useState, useRef } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, StyleSheet, FlatList, Animated, TouchableOpacity, Dimensions } from 'react-native';
import Homeitem from '../components/Homeitem';
import slides from '../slides';
import Indicator from '../components/Indicator';

export default Home = ({ navigation }) => {
  const { width } = Dimensions.get('window');

  const [currentIndex, setCurrentIndex] = useState(0);
  const ref = useRef(null);

  const scrollX = useRef(new Animated.Value(0)).current;

  const viewableItemsChanged = useRef(({ viewableItems }) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;

  // BUTTON
  const updateCurrentSlideIndex = (e) => {
    const contentOffsetX = e.nativeEvent.contentOffset.x;
    const currentIndex = Math.round(contentOffsetX / width);
    setCurrentIndex(currentIndex);
  };
  // NEXT SLIDE
  const goNextSlide = () => {
    const nextSlideIndex = currentIndex + 1;
    const offset = nextSlideIndex * width;
    ref?.current?.scrollToOffset({ offset });
  };

  const home = () => {
    const firstSlideIndex = slides.length - currentIndex;
    const offset = firstSlideIndex * currentIndex;
    ref?.current?.scrollToOffset({ offset });
    setCurrentIndex(firstSlideIndex);
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='blanchedalmond' />

      {/* FLATLIST */}

      <View style={{ flex: 3 }}>
        <FlatList

          onMomentumScrollEnd={updateCurrentSlideIndex}
          data={slides}
          renderItem={({ item }) => <Homeitem item={item} />}
          horizontal
          showsHorizontalScrollIndicator
          pagingEnabled
          bounces={false}
          onScroll={Animated.event([{
            nativeEvent: { contentOffset: { x: scrollX } }
          }], {
            useNativeDriver: false,
          })}
          scrollEventThrottle={32}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig}
          ref={ref}
        />
      </View>

      {/* FLUID INDICATOR */}
      <Indicator data={slides} scrollX={scrollX} />

      <View style={{ marginBottom: 50 }}>

        {/* CONTINUE BUTTON  and HOME BUTTON*/}

        {currentIndex == slides.length - 1 ?
          (<View style={{ height: 50 }}>
            <TouchableOpacity style={[styles.button, { backgroundColor: 'navy', borderWidth: 1 }]} onPress={home}>
              <Text style={{ fontWeight: 'bold', fontSize: 15 , color: 'white' }}>
                Home</Text>
            </TouchableOpacity>
          </View>
          ) : (
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={[styles.button, { backgroundColor: 'navy', borderWidth: 1 }]} onPress={goNextSlide}>
                <Text style={{ fontWeight: 'bold', fontSize: 15 , color: 'white'}}>
                  Continue</Text>
              </TouchableOpacity>
            </View>)
        }
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blanchedalmond',
  },
  button: {
    width: 100,
    height: 50,
    borderRadius: 5,
    backgroundColor: 'blanchedalmond',
    justifyContent: 'center',
    alignItems: 'center',
  },

});
