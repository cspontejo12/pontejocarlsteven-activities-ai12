export default [
    {
      id: '1',
      title: 'Why I choose the IT course?',
      description: 'Technology is best because it brings people together.”.',
      image: require('./assets/images/Augmented_reality.png'),
    },
    {
      id: '2',
      title: 'Tech-enthusiast',
      description: 'I want to know more about technology.',
      image: require('./assets/images/Dev_productivity.png'),
    },
    {
      id: '3',
      title: 'Internet',
      description: 'I want to know more of the thing that makes people connect online.',
      image: require('./assets/images/Noted.png'),
    },
    {
      id: '4',
      title: 'Fun',
      description: 'Whenever a technology topic is discussed, I always have fun learning and applying it .',
      image: require('./assets/images/Online_discussion.png'),
    },
    {
      id: '5',
      title: 'NO CHOICE',
      description: 'But the truth is, I did not have other course to pick.',
      image: require('./assets/images/Professor.png'),
    },
  ];
  