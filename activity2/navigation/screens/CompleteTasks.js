import * as React from 'react';
import { View, Text } from 'react-native';


export default function CompleteTasks({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'bisque' }}>
      <Text
        onPress={() => alert('Completed Task Go Here')}
        style={{ fontSize: 26, fontWeight: 'bold' }}>Completed Task Go Here</Text>
    </View>
  );
}